﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    class LongDeposit : Deposit,IProlongable
    {
        public LongDeposit(decimal Amount, int Period) : base(Amount, Period) { }
        public override decimal Income()
        {
            int month = Period;
            decimal balans = Amount;
            decimal totalIncome = 0;
            for (int i = 1; i <= month; i++)
            {
                if (i > 6)
                {
                    balans += (balans / 100) * 15;
                    totalIncome = balans - Amount;
                }
            }
            return totalIncome;
        }
        public new bool CanToProlong()
        {
            return (Period <= 36);
        }
    }
}
