﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    class Client : IEnumerable<Deposit>
    {
        private readonly Deposit[] deposits;
        public Client()
        {
            deposits = new Deposit[10];
        }
        public bool AddDeposit(Deposit deposit)
        {
            for (int i = 0; i < deposits.Length; i++)
            {
                if (deposits[i] is null)
                {
                    deposits[i] = deposit;
                    return true;
                }

            }
            return false;
        }
        public int CountPossibleToProlongDeposit()
        {
            int count = 0;
            foreach (var item in deposits)
            {
                if (item is IProlongable ip && ip.CanToProlong())
                {
                    count++;
                }
            }
            return count;
        }
        public IEnumerator<Deposit> GetEnumerator()
        {
            foreach (var deposit in deposits)
            {
                yield return deposit;
            }
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        public void SortDeposits()
        {
            Array.Sort(deposits, (x, y) => Comparer<Deposit>.Default.Compare(y, x));
        }

        public decimal TotalIncome()
        {
            decimal total = 0;
            for (int i = 0; i < deposits.Length; i++)
            {
                if (deposits[i] != (null))
                    total += deposits[i].Income();

            }
            return total;
        }
        public decimal MaxIncome()
        {
            decimal maxIcome = 0;
            for (int i = 0; i < deposits.Length; i++)
            {
                if (deposits[i] != null && maxIcome < deposits[i].Income())
                {
                    maxIcome = deposits[i].Income();
                }
            }
            return maxIcome;
        }
        public decimal GetIncomeByNumber(int number)
        {
            decimal incomeDeposit = 0;
            for (int i = 0; i < deposits.Length; i++)
            {
                if (deposits[i] != null && i== number-1)
                {
                    incomeDeposit = deposits[i].Income();
                }
                else
                {
                    if (deposits[i] == null)
                        incomeDeposit = 0;
                }
            }
            return incomeDeposit;
        }
    }
}
