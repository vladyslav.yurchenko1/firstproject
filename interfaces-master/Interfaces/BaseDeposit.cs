﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    class BaseDeposit : Deposit 
    {
        public BaseDeposit(decimal Amount, int Period) : base(Amount, Period) { }
        public override decimal Income()
        {
            int month = Period;
            decimal balans = Amount;
            decimal totalIncome = 0;
            for (int i = 1; i <= month; i++)
            {
                balans += (balans / 100) * 5;
                totalIncome = balans - Amount;
            }
            return totalIncome;
        }
        public new bool CanToProlong()
        {
            return false;
        }
    }
}
