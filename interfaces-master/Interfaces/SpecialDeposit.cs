﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    class SpecialDeposit : BaseDeposit, IProlongable
    {
        public SpecialDeposit(decimal Amount, int Period) : base(Amount, Period) { }
        public override decimal Income()
        {
            int month = Period;
            decimal balans = Amount;
            decimal totalIncome = 0;
            for (int i = 1; i <= month; i++)
            {
                balans += (balans / 100) * i;
                totalIncome = balans - Amount;
            }
            return totalIncome;
        }
        public new bool CanToProlong()
        {
            return (Amount > 1000m);
        }

    }
}
