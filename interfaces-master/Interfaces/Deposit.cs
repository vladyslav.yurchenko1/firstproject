﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    abstract class Deposit : IComparable<Deposit>
    {
        public decimal Amount { get; }
        public int Period { get; }
        public Deposit(decimal depositAmount, int depositPeriod)
        {
            Amount = depositAmount;
            Period = depositPeriod;
        }
        public abstract decimal Income();

        public bool CanToProlong()
        {
            return false;
        }
        public int CompareTo(Deposit other)
        {
            return Amount.CompareTo(other.Amount);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            var other = obj as Deposit;
            if (object.ReferenceEquals(other, null))
            {
                return false;
            }
            return this.CompareTo(other) == 0;
        }
        public static bool operator ==(Deposit left, Deposit right)
        {
            if (object.ReferenceEquals(left, null))
            {
                return object.ReferenceEquals(right, null);
            }
            return left.Equals(right);
        }
        public static bool operator >(Deposit left, Deposit right)
        {
            return Compare(left, right) > 0;
        }
        public static bool operator <(Deposit left, Deposit right)
        {
            return Compare(left, right) < 0;
        }
        public static bool operator <=(Deposit left, Deposit right)
        {
            return Compare(left, right) <= 0;
        }
        public static bool operator >=(Deposit left, Deposit right)
        {
            return Compare(left, right) >= 0;
        }
        private static int Compare(Deposit left, Deposit right)
        {
            throw new NotImplementedException();
        }

        public static bool operator !=(Deposit left, Deposit right)
        {
            return !(left == right);
        }
    }
}
