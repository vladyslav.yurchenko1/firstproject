﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deposit
{
    class BaseDeposit : Deposit
    {
        public BaseDeposit(decimal Amount, int Period) : base(Amount, Period) { }
        public override decimal Income()
        {
            int month = Period;
            decimal balans = Amount;
            decimal totalIncome = 0;
            for(int i =1;i<=month;i++)
            {
                
                balans += (balans / 100) * 5;
                totalIncome = balans - Amount;
            }
            return totalIncome;
        }
    }
}
