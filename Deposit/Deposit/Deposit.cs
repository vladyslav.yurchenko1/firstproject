﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deposit
{
    abstract class Deposit
    {
        public decimal Amount { get; }
        public int Period { get; }
        
        public Deposit( decimal depositAmount, int depositPeriod)
        {
            this.Amount = depositAmount;
            this.Period = depositPeriod;
        }
        public abstract decimal Income();

    }
}
