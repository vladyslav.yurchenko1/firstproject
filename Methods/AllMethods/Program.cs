﻿using System;


namespace Function
{
    public enum SortOrder { Ascending, Descending }
    public static class Function
    {


        public static bool IsSorted(int[] array, SortOrder order)
        {
            for (int i = 1; i < array.Length; i++)
                if (order == SortOrder.Ascending)
                {
                    if (array[i] < array[i - 1])
                    {
                        return false;
                    }
                }
                else
                {
                    if (array[i] > array[i - 1])
                    {
                        return false;
                    }
                }
            return true;
        }

        public static void Transform(int[] array, SortOrder order)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (IsSorted(array, order))
                {
                    array[i]+=GetIndex(array, i);
                }
            }
        }
        public static double SumGeometricElements(double a, double t, double alim = 0)
        {
            double temp;
            double nextValue;
            double sum = 0;
            while (a >= alim)
            {
                if (t > 0 && t < 1)
                {
                    temp = a;
                    nextValue = temp * t;
                    temp = a;
                    a = nextValue;
                    sum += temp;
                }
            }
            return sum;
        }
        public static double MultArithmeticElements(double a, double t, int n)
        {
            double temp;
            double nextValue;
            double mult = 1;
            for (int i = 0; i < n; i++)
            {
                temp = a;
                mult *= temp;
                nextValue = temp + t;
                a = nextValue;

            }
            return mult;
        }
        public static int GetIndex(int[] array, int indexOfArray)
        {
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[i] == array[j])
                    {
                        indexOfArray = array[i];
                    }
                }
                return indexOfArray;
            }
            return indexOfArray;
        }
    }
}

