﻿using System;

namespace MethosGeometr
{
    class Program
    {
        public static double SumGeometricElements(double a, double t, double alim=20)
        {
            double temp;
            double nextValue;
            double sum=0;
            while (a >= alim)
            {
                if (t > 0 && t < 1)
                {
                    temp = a;
                    nextValue = temp * t;
                    temp = a;
                    a = nextValue;
                    sum += temp;
                }
            }
            return sum;
        }
        static void Main(string[] args)
        {
            
            Console.WriteLine(SumGeometricElements(100,30));


        }
    }
}
