﻿using System;

namespace Methods
{
    class Program


    {
        public enum SortOrder { Ascending, Descending }
        public static bool IsSorted(int[] array, SortOrder order)
        {
            for (int i = 1; i < array.Length; i++)
                if (order == SortOrder.Ascending)
                {
                    if (array[i] < array[i - 1])
                    {
                        return false;
                    }
                }
                else
                {
                    if (array[i] > array[i - 1])
                    {
                        return false;
                    }
                }
                return true;
        }

        public static void Transform( int [] array, SortOrder order)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (IsSorted(array, order))
                {
                    array[i] += i;
                }
            }   
        }

        static void Main(string[] args)
        {
            int[] array = { 0, 0, 0, 0 };
            for (int i = 0; i < array.Length; i++)
            {
                if (IsSorted(array, SortOrder.Ascending))
                {
                    array[i] += i;
                    i+=1;
                }
                Console.WriteLine(array[i]);
            }
        }

    }   
}
