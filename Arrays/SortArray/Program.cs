﻿using System;

namespace SortArray
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the number of rows");
            int n = int.Parse(Console.ReadLine());
            int[,] myArray = new int[n,n] ;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    myArray[i, j] = int.Parse(Console.ReadLine());
                }
            }
            
            for (int i = 0; i < myArray.GetLength(0); i++)
            {
                for (int j = 0; j < myArray.GetLength(1); j++)
                {
                    if (i > j)
                    {
                        myArray[i,j] = 0;
                    }
                    else
                    {
                        if (j > i)
                        {
                            myArray[i, j] = 1;
                        }

                    }
                    Console.Write($"{myArray[i, j]} ");
                }
                Console.WriteLine();
            }
        }
    }
}
