﻿using System;

namespace Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[] myArray = new int[n] ;
            int temp;
            for (int i = 0; i < myArray.Length; i++)
            {
                myArray[i] = int.Parse(Console.ReadLine());
            }
            for (int i = 0; i < myArray.Length/2; i++)
            {
                if (myArray[i] % 2 == 0 && myArray[myArray.Length - i - 1]%2==0)
                {
                    temp = myArray[i];
                    myArray[i] = myArray[myArray.Length - i - 1];
                    myArray[myArray.Length - i - 1] = temp;
                }
                
            }
            for(int i= 0; i < myArray.Length; i++)
            {
                Console.WriteLine(myArray[i]);
            }

        }
    }
}
