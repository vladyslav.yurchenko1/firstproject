﻿using System;
using System.Linq;

namespace FindRangBetweenNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[] myArray = new int[n];
            int result;
            for (int i = 0; i < n; i++)
            {
                myArray[i] = int.Parse(Console.ReadLine());  
            }
            if (myArray.Length != 0)
            {
                int maxValue = myArray.Max();
                int firstEntry = Array.FindIndex(myArray, i => i == maxValue);
                int lastEntry = Array.FindLastIndex(myArray, i => i == maxValue);
                result = lastEntry - firstEntry;
            }
            else
            {
                result = 0;
            }
            Console.WriteLine(result);
           
        }
    }
}
