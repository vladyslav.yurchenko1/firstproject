﻿using System;

namespace ByteCounter
{
    class Program
    {
        static void Main(string[] args)
        {
           
            int decimalNumber = int.Parse(Console.ReadLine());
            int count = 0;
            
            while (decimalNumber > 0)
            {
                if (decimalNumber % 2 == 1)
                {
                    count++; 
                }
                decimalNumber = decimalNumber / 2;
            }
            Console.WriteLine(count);
        }
    }

}



