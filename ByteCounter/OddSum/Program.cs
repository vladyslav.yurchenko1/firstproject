﻿using System;

namespace OddSum
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = int.Parse(Console.ReadLine());
            /*
            int number;
            */
            int sumOfDigitsOfOdd = 0;
            int lastNumber;
            while (number > 0)
            {
                lastNumber = number % 10;
                if (lastNumber % 2 != 0)
                {
                    sumOfDigitsOfOdd += lastNumber;
                }
                number /= 10;
            }
            Console.WriteLine(sumOfDigitsOfOdd);
        }
    }
}
