﻿using System;

namespace numberFibbonachi
{
    class Program
    {
        static void Main(string[] args)
        {
            int numberFib=int.Parse(Console.ReadLine());
            int currentFib = 0;
            int tempNumber;
            int sumOfFib = 0;
            int nextFib = 1;
            for (int i = 0; i < numberFib; i++)
            {
                tempNumber = currentFib;
                currentFib = nextFib;
                nextFib = tempNumber + nextFib;
                sumOfFib += tempNumber;
            }
            Console.WriteLine(sumOfFib);
        }
    }
}
