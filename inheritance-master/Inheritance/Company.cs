﻿using System;
using System.Collections.Generic;
using System.Text;


namespace InheritanceTask
{

    class Company
    {
        private readonly Employee[] employees;


        public Company(Employee[] employees)
        {
            this.employees = employees;
        }
        public void GiveEverybodyBonus(decimal companyBonus)
        {
            for (int i = 0; i < employees.Length; i++)
            {
                employees[i].SetBonus(companyBonus);
            }
        }
        public decimal TotalToPay()
        {
            decimal total = 0;
            for (int i = 0; i < employees.Length; i++)
            {
                total += employees[i].ToPay();

            }
            return total;
        }
        public string NameMaxSalary()
        {
            var employee = new Employee();
            decimal maxSalary = 0;
            for (int i = 0; i < employees.Length; i++)
            {
                var salary = employees[i].ToPay();
                if (salary > maxSalary)
                {
                    maxSalary = salary;
                    employee = employees[i];
                }
            }
            return employee.Name;

        }
    }

}