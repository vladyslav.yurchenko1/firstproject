﻿using System;

namespace Condition
{
    public static class Condition
    {
        public static int Task1(int n)
        {
            int n = int.Parse(Console.ReadLine());
            int firstValue;
            int secondValue;
            int thirdValue;
            int result;
            if (n >= 100 && n <= 999)
            {
                firstValue = n / 100;
                secondValue = n / 10 % 10;
                thirdValue = n % 10;
                if (firstValue >= secondValue && firstValue >= thirdValue)
                {

                    if (secondValue >= thirdValue)
                    {
                        n = (firstValue * 100 + secondValue * 10 + thirdValue);
                        return n;
                    }
                    else
                    {
                        result = (firstValue * 100 + thirdValue * 10 + secondValue);
                        Console.WriteLine(result);
                    }
                }
                else if (secondValue >= firstValue && secondValue >= thirdValue)
                {
                    if (firstValue >= thirdValue)
                    {
                        result = (secondValue * 100 + firstValue * 10 + thirdValue);
                        Console.WriteLine(result);
                    }
                    else
                    {
                        result = (secondValue * 100 + thirdValue * 10 + firstValue);
                        Console.WriteLine(result);
                    }
                }
                else
                {
                    if (firstValue >= secondValue)
                    {
                        result = (thirdValue * 100 + firstValue * 10 + secondValue);
                        Console.WriteLine(result);

                    }
                    else
                    {
                        result = (thirdValue * 100 + secondValue * 10 + firstValue);
                        Console.WriteLine(result);
                    }
                }
            }



        }
    }
}