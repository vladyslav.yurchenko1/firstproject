﻿using System;
using System.Linq;

namespace ArrayObject
{
    public static class ArrayTasks
    {
        public static void ChangeElementsInArray(int[] nums)
        {
            int temp;
            for (int i = 0; i < nums.Length / 2; i++)
            {
                if (nums[i] % 2 != 1 && nums[nums.Length - i - 1]%2!=1)
                {
                    temp = nums[i];
                    nums[i] = nums[nums.Length - i - 1];
                    nums[nums.Length - i - 1] = temp;
                }

            }
            for (int i = 0; i < nums.Length; i++)
            {

            }
        }
        public static int DistanceBetweenFirstAndLastOccurrenceOfMaxValue(int[] nums)
        {
            int result;
            if (nums.Length > 0)
            {
                int maxValue = nums.Max();
                int firstEntry = Array.FindIndex(nums, i => i == maxValue);
                int lastEntry = Array.FindLastIndex(nums, i => i == maxValue);
                result = lastEntry - firstEntry;
            }
            else
            {
                result = 0;
            }
            return result;
            
        }
        public static void ChangeMatrixDiagonally(int[,] matrix)
        {
            for(int i = 0; i < matrix.GetLength(0); i++)
            {
                for(int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (i > j)
                    {
                        matrix[i, j] = 0;
                    }
                    else
                    {
                        if(i < j)
                        {
                            matrix[i, j] = 1;
                        }
                    }
                }
            } 
        }
    }
}
