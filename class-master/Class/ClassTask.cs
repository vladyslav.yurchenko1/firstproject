using System;
using System.Collections.Generic;
using System.Linq;

namespace Class 
{ 
    class Rectangle
    {
        private double sideA;
        private double sideB;

        public Rectangle(double a, double b)
        {
            this.sideA = a;
            this.sideB = b;
        }
        public Rectangle(double a)
        {
            this.sideA = a;
            this.sideB = 5;
        }
        public Rectangle()
        {
            this.sideA = 4;
            this.sideB = 3;
        }
        public double GetSideA()
        {
            return sideA;
        }
        public double GetSideB()
        {
            return sideB;
        }
        public double Area()
        {
            return sideA * sideB;
        }
        public double Perimeter()
        {
            return sideA + sideA + sideB + sideB;
        }
        public bool IsSquare()
        {
            if (sideA == sideB)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
       
        public void ReplaceSides()
        {
            sideB = GetSideA();
            sideA = GetSideB();
        }
             
}
    class ArrayRectangles
    {
        private readonly Rectangle[] rectangle_array;
        public ArrayRectangles(int n)
        {
            rectangle_array = new Rectangle[n];
        }
        public ArrayRectangles(Rectangle[] rectangle_array)
        {
            this.rectangle_array = rectangle_array;
        }
        public bool AddRectangle(Rectangle rectangle)
        {
            for (int i = 0; i < rectangle_array.Length; i++)
            {
                if (rectangle_array[i] == null)
                {
                    rectangle_array[i] = rectangle;
                    return true;
                }

            }
            return false;
        }
        public interface IEnumerable
        {

        }
        public int NumberMaxArea()
        {
            int indexMax = 0;
            double maxArea = rectangle_array[0].Area();
            for (int i = 1; i < rectangle_array.Length; i++)
            {
                if ( maxArea < rectangle_array[i].Area())
                {
                    maxArea = rectangle_array[i].Area();
                    indexMax = i;
                }
            }
            return indexMax;
        }

        public int NumberMinPerimeter()
        {
            int indexMin = 0;
            double perimeterMin = rectangle_array[0].Perimeter();
            for (int i = 0; i < rectangle_array.Length; i++)
            {
                if (rectangle_array[i].Perimeter() < perimeterMin)
                {
                    perimeterMin = rectangle_array[i].Perimeter();
                    indexMin = i;
                }
            }
            return indexMin;
        }
        public int NumberSquare()
        {
            int countOfSquares = 0;
            for (int i = 0; i < rectangle_array.Length; i++)
            {
                if (rectangle_array[i].IsSquare())
                {
                    countOfSquares++;
                }
            }
            return countOfSquares;
        }

    }
}
