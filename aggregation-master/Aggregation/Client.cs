using System;

namespace Aggregation
{
    class Client
    {
        private readonly Deposit[] deposits;
        public Client()
        {
            deposits = new Deposit[10];
        }
        public bool AddDeposit(Deposit deposit)
        {
            for (int i = 0; i < deposits.Length; i++)
            {
                if (deposits[i] == null)
                {
                    deposits[i] = deposit;
                    return true;
                }

            }
            return false;
        }
        public decimal TotalIncome()
        {
            decimal total = 0;
            for (int i = 0; i < deposits.Length; i++)
            {
                if(deposits[i]!=null)
                total += deposits[i].Income();

            }
            return total;
        }
        public decimal MaxIncome()
        {
            decimal maxIcome = 0;
            for (int i = 0; i < deposits.Length; i++)
            {
                if (deposits[i] != null && maxIcome < deposits[i].Income())
                {
                        maxIcome = deposits[i].Income();
                }
            }
            return maxIcome;
        }
        public decimal GetIncomeByNumber(int number)
        {
            decimal incomeDeposit = 0;
            for (int i = 0; i < deposits.Length; i++)
            {
                if (deposits[i]!=null && i+1==number)
                {
                    incomeDeposit = deposits[i].Income();
                }
                else
                {
                    if(deposits[i]==null)
                    incomeDeposit = 0;
                }
            }
            return incomeDeposit;
        }
    }
}