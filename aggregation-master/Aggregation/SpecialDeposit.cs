namespace Aggregation
{
    class SpecialDeposit : BaseDeposit
    {
        public SpecialDeposit(decimal Amount, int Period) : base(Amount, Period) { }
        public override decimal Income()
        {
            int month = Period;
            decimal balans = Amount;
            decimal totalIncome = 0;
            for (int i = 1; i <= month; i++)
            {
                balans += (balans / 100) * i;
                totalIncome = balans - Amount;
            }
            return totalIncome;
        }
    }
}