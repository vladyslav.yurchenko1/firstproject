﻿using System;
using System.Runtime.Serialization;

namespace Exceptions
{
    public class MatrixException : Exception
    {
        public MatrixException()
        {
        }
        public override string Message
        {
            get
            {
                throw new Exception();
            }
        }

        public MatrixException(string message) : base(message)
        {
        }

        public MatrixException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected MatrixException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    public class Matrix
    {
        public int Rows
        {
            get;
            set;
        }
        public int Columns
        {
            get;
            set;
        }

        public double[,] Array
        {
            get;
            set;
        }

        public Matrix(int rows, int columns)
        {
            if (rows <= 0 || columns <= 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            else
            {
                this.Rows = rows;
                this.Columns = columns;
                this.Array = new double[rows, columns];
            }
        }
        public Matrix(double[,] array)
        {
            if (array != null)
            {
                this.Rows = array.GetLength(0);
                this.Columns = array.GetLength(1);
                this.Array = array;
            }
            else throw new ArgumentNullException();
        }

        public double this[int row, int column]
        {

            get
            {
                if (row < 0 || row >= Rows || column < 0 || column >= Columns)
                    throw new ArgumentException();
                else return Array[row, column];
            }
            set
            {
                if (row < 0 || row >= Rows || column < 0 || column >= Columns)
                    throw new ArgumentException();
                else Array[row, column] = value;

            }
        }

        public Matrix Add(Matrix matrix)
        {
            var addictionMatrix = new Matrix(Array);
            if (matrix == null || addictionMatrix == null)
            {
                throw new ArgumentNullException();
            }
            if (Columns != matrix.Columns || Rows != matrix.Rows || Columns != addictionMatrix.Columns || Rows != addictionMatrix.Rows)
                throw new MatrixException();
            for (int row = 0; row < Rows; row++)
            {
                for (int column = 0; column < Columns; column++)
                {
                    addictionMatrix[row, column] += matrix.Array[row, column];
                }
            }
            return addictionMatrix;
        }

        public Matrix Subtract(Matrix matrix)
        {
            var subtractMatrix = new Matrix(Array);
            if (matrix == null || subtractMatrix == null)
            {
                throw new ArgumentNullException();
            }
            if (Columns != matrix.Columns || Rows != matrix.Rows || Columns != subtractMatrix.Columns || Rows != subtractMatrix.Rows)
                throw new MatrixException();
            for (int row = 0; row < Rows; row++)
            {
                for (int column = 0; column < Columns; column++)
                {
                    subtractMatrix[row, column] -= matrix.Array[row, column];
                }
            }
            return subtractMatrix;
        }
        public Matrix Multiply(Matrix matrix)
        {
            var multOfMatrix = new Matrix(Array);
            if (matrix == null)
            {
                throw new ArgumentNullException();
            }
            if (Columns != matrix.Columns || Rows != matrix.Rows)
                throw new MatrixException();
            for (int row = 0; row < Rows; row++)
            {
                for (int column = 0; column < Columns; column++)
                {
                    double sum = 0.0;
                    for (int k = 0; k < Rows; k++)
                    {
                        sum += matrix.Array[k, column] * matrix.Array[row, k];
                        multOfMatrix[row, column] = sum;
                    }
                }
            }
            return multOfMatrix;
        }

    }
}
