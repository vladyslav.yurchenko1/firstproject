﻿using System;


namespace Function
{
    public enum SortOrder { Ascending, Descending }
    public static class Function
    {


        public static bool IsSorted(int[] array, SortOrder order)
        {
            for (int i = 1; i < array.Length; i++)
                if (order == SortOrder.Ascending)
                {
                    if (array[i] < array[i - 1])
                    {
                        return false;
                    }
                }
                else
                {
                    if (array[i] > array[i - 1])
                    {
                        return false;
                    }
                }
            return true;
        }

        public static void Transform(int[] array, SortOrder order)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (IsSorted(array, order))
                {
                    for (i = 0; i < array.Length; i++)
                    {
                        int index = i;
                        array[i] += index;
                    }

                }
            }
        }
        public static double SumGeometricElements(double a, double t, double alim = 0)
        {
            double sum = 0;
            while (a >= alim)
            {
                if (t > 0 && t < 1)
                {                  
                    sum += a;
                    a *= t;

                }
            }
            return sum;
        }
        public static double MultArithmeticElements(double a, double t, int n)
        {
            double nextValue;
            double mult = 1;
            for (int i = 0; i < n; i++)
            {              
                mult *= a;
                a += t;

            }
            return mult;
        }
        
    }
}

